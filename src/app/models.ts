export class Stats {
    entity: string;
    votes: number;
    ip?: string;
}