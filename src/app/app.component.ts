import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Stats } from './models';
import { interval } from "rxjs/internal/observable/interval";
import { startWith, switchMap } from "rxjs/operators";
import { createRendererV1 } from '@angular/core/src/view/refs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = '';
  apiConnectivity = false;
  dbConnectivity = false;
  version = "v1";
  logged = false;
  

  constructor(private http: HttpClient) {
    this.Get();
    this.StartTimer();
  }

  api = [];


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  httpOptionsJohn = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'end-user': 'john'})
  };

  LogIn() {
    this.logged = true;
    this.title = 'user logged in';
  }

  LogOut() {
    this.logged = false;
    this.title = '';
  }



  view: any[] = [800, 400];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = false;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3']
  };

  flame = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  
  stats = [];

  single = [
    {
      "name": "NSX DC",
      "value": 0
    },
    {
      "name": "NSX Cloud",
      "value": 0
    },
    {
      "name": "HCX",
      "value": 0
    },
    {
      "name": "AppDefense",
      "value": 0
    },
    {
      "name": "vRNI",
      "value": 0
    },
    {
      "name": "Velocloud",
      "value": 0
    }
  ]; 

  Vote(entity: string) {
    if (this.logged) {
      return this.http.post('/api/vote', { entity: entity }, this.httpOptionsJohn);
    }
    else {
      return this.http.post('/api/vote', { entity: entity }, this.httpOptions);
    }
  }

  GetVotes(): Observable<any> {
    if (this.logged) {
      return this.http.get<any>('/api/vote', this.httpOptionsJohn);
    }
    else {
      return this.http.get<any>('/api/vote', this.httpOptions);
    }  
  }

  Get() {
    var tmp = [];
    this.GetVotes().subscribe(stats => {
      this.apiConnectivity = true;
      this.dbConnectivity = true;
      this.version = stats.metadata.version;
      stats.votes.forEach(function(value, index, array) {
        //console.log(value);
        //console.log(index);
        //console.log(array);
        tmp.push({"name": value.entity, "value": value.votes, "ip": value.ip});
      });
      console.log(tmp)
      this.single = tmp;


      var found = false;
      var i = 0;
      var index2 = 0;
      this.api.forEach(function(element) {
        element.label = "label-warning";
        if (element.hostname == stats.metadata.hostname) {
          found = true;
          element = stats.metadata;
          index2 = i;
        }
        i++; 
        console.log(index2);
      });

      if (found == false) {
        var le = this.api.push(stats.metadata)
        index2 = le-1;
      }

      this.api[index2].label = "label-info";
  
      console.log(found)
      console.log(stats.metadata)
      console.log(this.api)

    },
    err => {
      console.log(err.status);
      switch(err.status) {
        case 0: {this.apiConnectivity = false; this.dbConnectivity = false; break;}
        case 502: {this.apiConnectivity = false; this.dbConnectivity = false; break;}
        case 503: {this.apiConnectivity = false; this.dbConnectivity = false; break;}
        case 504: {this.apiConnectivity = false; this.dbConnectivity = false; break;} 
        case 500: {this.apiConnectivity = true; this.dbConnectivity = false; break;}
        default: console.log(err.status)        
      }
      console.log(this.apiConnectivity);
      console.log(this.dbConnectivity);
    } );  
  }

  StartTimer() {
    const subscription = interval(500).pipe(startWith(0)).subscribe(
      {
        next: () => {
          let nb = Math.floor(Math.random() * 6);
          switch(nb) {
            case 0: {this.Vote("NSX DC").subscribe(); break;};
            case 1: {this.Vote("NSX Cloud").subscribe(); break;};
            case 2: {this.Vote("HCX").subscribe(); break;};
            case 3: {this.Vote("AppDefense").subscribe(); break;};
            case 4: {this.Vote("vRNI").subscribe(); break;};
            case 5: {this.Vote("Velocloud").subscribe(); break;};
          }
          this.Get()
         },
        error: err => {},
        complete: () => console.log('complete')
      });
  }

  Reset() {
    this.api = [];
    console.log("reset")
    return this.http.delete('/api/vote', {}).subscribe();
  }

}
